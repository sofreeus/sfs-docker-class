> From: ettiejr@ettiepy.com<br>
> Subject: Devs need the web server...
>
>   Great job on the web server!  We've got some remote web designers who need access to the image you created.  Can you push it up to DockerHub so they can grab their own copy?
>
> You're the best!
>
> EJ

---

1. Log in to [Docker Hub](https://hub.docker.com) - Web
2. `docker login` to log in to Docker Hub at the command line
3. Create the repository using the web interface
4. Tag your image with `docker tag joke-site <docker_hub_username>/joke-site`
5. Push the image with `docker push <docker_hub_username>/joke-site`
6. Pull and run your partner's image!
7. View your published image in the repository at hub.docker.com.

---

hub.docker.com is not the only registry

try these:

docker pull public.ecr.aws/docker/library/node:lts-alpine3.19

docker pull registry.gitlab.com/sofreeus/sfs-sysops:latest

---

docker is not the only container runtime / userspace utility

try all things with podman:

podman pull registry.gitlab.com/sofreeus/sfs-sysops:latest

podman pull debian

podman pull alpine

---

see also: debian+podman:/etc/containers/registries.conf.d/shortnames.conf
