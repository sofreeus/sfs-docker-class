> From: ettiejr@ettiepy.com<br>
> Priority: High<br> 
> Subject: Shut It Down!
>
>   We have TOO MANY containers running! Please shut down any from the previous labs!

---

1. Use `docker stop` or `docker kill` to shut down the container from the last lab.
2. What does `docker rm --force` do again?


Bonus:
- Feel free to restart the container (see the last lab) and try both commands.
