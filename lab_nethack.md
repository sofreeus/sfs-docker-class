> From: ettiejr@ettiepy.com<br>
> Subject: Nethack Tournament
>
>   Some of us play in a monthly Nethack tournament.  You know Nethack, right?  It's only the best video game ever made.
>
>   If you've never played before, you'll want to keep the [commands](http://www.nethack.org/v360/Guidebook.html#_TOCentry_8) handy.  You can check out the [NetHackWiki](https://nethackwiki.com/wiki/Main_Page) for a protracted explanation of the game.
>
>   Anyway, I've attached a Dockerfile that I use to create a Nethack image.  You probably just need to change the options and you'll be good to go.
>
>   Race you to the Amulet of Yendor!
>
> See you Friday night!
>
> EJ

---

Dockerfile:

```Dockerfile
FROM ubuntu:20.04

RUN apt update && \
    DEBIAN_FRONTEND=noninteractive apt upgrade -y && \
    DEBIAN_FRONTEND=noninteractive apt install -y nethack-console && \
    apt clean

ENV NETHACKOPTIONS name:EJ,catname:Mao,dogname:Doug,align:lawful,role:Valkyrie,gender:random

CMD /usr/games/nethack
```

---

1. Save the Dockerfile in an empty directory - it must be called `Dockerfile`
2. Navigate to that directory on the command line
3. Update your options!
    - `name`, `catname`, and `dogname` can be anything you want (though special characters might be problematic)
    - `align` (alignment) can be: `lawful`, `neutral`, or `chaotic`
    - `role` can be one of: `Archeologist`, `Barbarian`, `Caveman`, `Healer`, `Knight`, `Monk`, `Priest`, `Ranger`, `Rogue`, `Samurai`, `Tourist`, `Valkyrie`, or `Wizard`
    - `gender` can be: `female`, `male`, or `random`
4. Build the image with `docker build --tag mynethack .` <<< Don't forget the `.`
5. Play Nethack!!!  `docker run -it mynethack` (Type `#quit` from within the game to quit, Quitter.)
