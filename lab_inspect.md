> From: ettiejr@ettiepy.com<br>
> Priority: High
>
> Subject: Need Details About Container
>
>   Sorry to thrust this on you.  I know you're not up to speed yet.  But we've got a container running in production, and we're not sure what it is.  Can you use `docker inspect` to take a look?
>
> Thanks!
>
> EJ

---

1. Run this on your machine: `docker run -d -p 8080:8080 --name mystery_machine dkeis/fortune`
2. What can you find out about it?

---

1. Pull down this image: `docker pull netflixoss/sketchy-api:1.0.0`
2. Is that something you'd want to run?
