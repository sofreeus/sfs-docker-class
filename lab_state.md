> From: ettiejr@ettiepy.com<br>
> Subject: Stateful Data
>
>   Passing configurations into a container is great, but we have some cases where we need a little more.  A mounted volume can hold data while a container is offline, which is really handy for storing logs after a container exits - or even keeping database files.
>
> -EJ

---

1. Create a local directory called *myDataDir* on your machine: `mkdir myDataDir`
2. Use the `--volume $(pwd)/myDataDir:/data` option with `docker run -it ubuntu /bin/bash` to check it out.
3. In the container, write a file in the /data directory.
4. Ctrl+D to log out of the container.
5. Look in `myDataDir`.  What's there?
6. Look at the output of `docker ps`.  Is your container running?
7. Start a new container using the same docker command (with the `--volume` option)
8. What's in the `/data` directory of your new container?

Bonus, let's check the cgroup and namespace configuration for this new volume:
1. Let's start a container with a process we can easily determine `$ docker run  --detach --volume $(pwd)/myDataDir:/data ubuntu sleep 600`
2. And let's find that PID. `$ ps ax | grep "sleep 600"`
3. We can find the namespace for the volume mount with our trusty `readlink` command. `$ sudo readlink /proc/$PID/ns/mnt`
4. And we can find information about our mount by checking `/proc`. `$ cat /proc/$PID/mountinfo | grep myDataDir`
