You've just started work at a new company, Aunt Ettie's House of Py.  You've got a good handle on the rest of your job, but they're a Docker shop and you've never used Docker before.  They knew that when they hired you, and they've allocated some ramp-up time for you to get familiar with Docker before jumping in feet first.

---

> From: ettiejr@ettiepy.com<br>
> Subject: Getting Started With Docker
>
> Hey, you,
>
>   Welcome!  I hope your first days is going well so far.  As discussed previously, you'll need to start learning Docker!  For today, head on over to cloud.google.com and load up a web shell. Or if you prefer, a local shell but make sure docker is installed locally
>
> Let me know if you have any trouble.
>
> -EJ

---

1. Open your local shell (w/ Docker installed) or head over to [Google Cloud Shell](https://shell.cloud.google.com) and get logged in.
2. Create a new directory to work in. `$ mkdir -p sfs-docker-class`
3. Check that docker is installed. `$ docker version`
4. If Docker is not installed, [install it](https://docs.docker.com/get-docker/), or use [Google Cloud Shell](https://shell.cloud.google.com)
