# Introduction to Docker

!

$ whoami
![whoami](images/devops-camp-hiking_w_aaron.jpg)

!

# Introduction

- What is Docker?
- Why use Docker?

<!-- Docker and 12-Factor design -->

<!-- No persistent storage! -->

!

Why are containers better than bare processes?

- isolation of config/data/binary storage
  (easy to drop in, easy to remove)

- ability to easily limit resource consumption

!

### "The Diagram"

![The Diagram](images/virtualization-vs-containers.png)

!

# Why are containers better than VMs?

Improvements in:

- processor efficiency
- memory efficiency
- storage efficiency
- performance (no virt penalty / hardware speed)

!

When are containers **NOT** better than VMs?

- workload needs graphics
- workload is likely to be hostile

!

## Terms

- Registry (Docker Hub, GitLab) vs. Repository (ubuntu)
- Image (debian) vs. Container (smiling_walrus)
- Tags (0.0.1, latest, hash)
- Push and Pull

<!-- No lab. -->

!

## Installation

- You may do the exercises on your local machine or in Google Cloud Shell.
- Open up a local shell if you have Docker installed (or you're comfortable installing it), or log into your Google Cloud account and open up a web shell where Docker is pre-installed.

<a href="https://gitlab.com/sofreeus/sfs-docker-class/blob/master/lab_installation.md" target="_blank">Installation Lab</a>

<!-- Lab: lab_installation.md -->

!

## Running a Container

- must reference an image
- interactive vs detached
- name (autogenerates unless specified with `--name`)
- simple: `docker run ubuntu`
- complex: `docker run --detach --env "ENVAR=value" --publish 8000:80 --name mysrv myapp:0.0.1`

<a href="https://gitlab.com/sofreeus/sfs-docker-class/blob/master/lab_running.md" target="_blank">Running Lab</a>

<!-- Lab: lab_running.md -->
<!-- Discuss interactive vs. detached -->

!

## Exposing Ports

- Containers are not accessible, by default, to other hosts

<a href="https://gitlab.com/sofreeus/sfs-docker-class/blob/master/lab_ports.md" target="_blank">Ports Lab</a>

!

## Passing Config

- Environment variables
- Environment files

<a href="https://gitlab.com/sofreeus/sfs-docker-class/blob/master/lab_configs.md" target="_blank">Configs Lab</a>

!

## Stateful Data (or Configs)

- Don't count on container data
- Mounting volumes

<a href="https://gitlab.com/sofreeus/sfs-docker-class/blob/master/lab_state.md" target="_blank">State Lab</a>

!

## Stopping

- `docker stop`
- `docker kill`
- `docker rm --force`

<a href="https://gitlab.com/sofreeus/sfs-docker-class/blob/master/lab_stop.md" target="_blank">Stop Lab</a>

!

## Cleaning Up

- `docker ps --all`
- `docker images`
- `docker {object} prune` : container, image, system
- `docker rm` and `docker rmi`

<a href="https://gitlab.com/sofreeus/sfs-docker-class/blob/master/lab_cleanup.md" target="_blank">Cleanup Lab</a>

!

## Building Your Own

- Dockerfile
- Context
- `docker build <context>`

<a href="https://gitlab.com/sofreeus/sfs-docker-class/blob/master/lab_nethack.md" target="_blank">Nethack Lab</a>

<a href="https://gitlab.com/sofreeus/sfs-docker-class/blob/master/lab_webserver.md" target="_blank">Webserver Lab</a>

!

## Pushing to DockerHub

- Sharing with others
- Facilitating deployments
- Moving an image

<a href="https://gitlab.com/sofreeus/sfs-docker-class/blob/master/lab_push.md" target="_blank">Push Lab</a>

!

## Resources

- Alice Goldfuss, The Container Operator's Manual:  [YouTube](https://www.youtube.com/watch?reload=9&v=sJx_emIiABk)
- Samuel Karp, Linux Container Primitives: cgroups, namespaces, and more!: [YouTube](https://www.youtube.com/watch?v=x1npPrzyKfs&feature=emb_logo)
- Original Course by Aaron Brown, @aayore.

!

## The End

- Thank you!

## Questions?

- What should we put in the next Docker class?

!

# Appendix

!

## What's a cgroup?

- A Cgroup limits what resources a process (container) has access to
  - CPU
  - Memory
  - Devices
  - Number of PIDs
  - etc.

`tree /sys/fs/cgroup`


[Lab](https://gitlab.com/sofreeus/sfs-docker-class/blob/master/lab_cgroups.md)

<!-- Lab: lab_cgroups.md -->

!

## What's a Namespace?

- A namespace limits what a process can share resources with
  - Network
  - Filesystem Mounts
  - Processes
  - UIDs/Group IDs
  - etc.

[Lab](https://gitlab.com/sofreeus/sfs-docker-class/blob/master/lab_netns.md)


<!-- docker run uses separate network namespace per container -->
<!-- Lab: lab_netns.md -->

!

## Artifacts

- Commit hash
- Software package
- Container

<!-- Lab: ??? -->

!

## Layers, Tarballs, and Union Filesystems?

```
Step 1/4 : FROM ubuntu:20.04
20.04: Pulling from library/ubuntu
6a5697faee43: Pull complete
ba13d3bc422b: Pull complete
a254829d9e55: Pull complete
Digest: sha256:fff16eea1a8ae92867721d90c59a75652ea66d29c05294e6e2f898704bdb8cf1
Status: Downloaded newer image for ubuntu:20.04
```

!

### Several Low-Level Linux Features "Stapled Together"
- Control Groups (CGroups)
- Namespaces
- Union Filesystems
