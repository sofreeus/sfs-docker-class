# SFS Docker Class

### Prerequisites

Create an account at [Docker Hub](https://hub.docker.com/)

### Presentation

An up-to-date version of the presentation can be viewed at https://sofreeus.gitlab.io/sfs-docker-class/

To build and view the presentation locally:

```
docker build -t presentation .
docker run -d -p 8080:80 presentation
```

And open http://localhost:8080
