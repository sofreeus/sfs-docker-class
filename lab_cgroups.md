> From: ettiejr@ettiepy.com<br>
> Subject: But How Does It Work?
>
> Good afternoon,
>
>   It's great that you've got a container running, but if containers are just processes, what makes them different from any other process running on our servers? Can you help me understand cgroups and how they limit processes?
>
> Good luck,
>
> EJ

---

1. The cgroup settings for a linux system live at `/sys/fs/cgroup/`. Let's look at `$ cat /sys/fs/cgroup/cpu/cpu.shares` to see how much cpu time the root cgroup is limited to.
2. Let's take a look at the maximum number of Process IDs allowed for the root cgroup as well.
3. Let's run a container that puts limits on both of those resources `$ docker run --name lab_cgroups --cpu-shares 256 --pids-limit 4 --detach ubuntu sleep 600`
4. Docker organizes cgroups for containers into a `docker` subdirectory. Let's go find it. `$ ls /sys/fs/cgroup/cpu/docker`
5. What is the CPU share limit for that cgroup? `cat /sys/fs/cgroup/cpu/docker/$CONTAINER_ID/cpu.shares`
6. What about max PIDs?

---

Bonus: Now, let's see things from the perspective of the container.

1. Let's go inside the container and see what cpu.shares for the root cgroup is set to. `$ docker exec lab_cgroups cat /sys/fs/cgroup/cpu/cpu.shares`
2. What about max PIDs?