FROM httpd

ENV DEBIAN_FRONTEND=noninteractive

RUN apt update && \
    apt install -y unzip

ADD https://github.com/jsakamoto/MarkdownPresenter/archive/master.zip /

RUN unzip -j /master.zip -d /usr/local/apache2/htdocs

COPY presentation.md /usr/local/apache2/htdocs

COPY images/ /usr/local/apache2/htdocs/images/

RUN rm htdocs/index.html && \
    ln -s /usr/local/apache2/htdocs/Presenter.html /usr/local/apache2/htdocs/index.html
