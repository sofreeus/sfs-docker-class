> From: ettiejr@ettiepy.com<br>
> Subject: Running Containers
>
> Good afternoon,
>
>   Since you've gotten Docker installed, it's time to try and run something.  Since pretty much everything starts with a Hello World app, go ahead and give it a try with a local Docker instance.
>
> Good luck,
>
> EJ

---

1. Try `docker help run` at the command line
1. Use `docker search` to find an ubuntu image [note: you can also search in a browser: https://hub.docker.com]
1. Run `docker pull ubuntu`
1. Run `docker images`
1. Run `docker run ubuntu echo "i am container"`
1. Run `docker ps` is your container still running?

Bonus:
- What other commands can you run besides echo on your ubuntu container? `docker run ubuntu whoami`?


[official Docker images](https://docs.docker.com/docker-hub/official_images/#:~:text=The%20Docker%20Official%20Images%20are,for%20the%20majority%20of%20users)
