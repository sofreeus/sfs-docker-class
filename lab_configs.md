> From: ettiejr@ettiepy.com<br>
> Subject: Managing Configurations
>
>   We do our best to practice [Twelve Factor](https://12factor.net) application design.  Docker helps us with the third factor, separating our config from our app.  Take some time today and figure out how to pass environment variables and environment files into a Docker container.
>
> -EJ

---

1. What happens if you run `docker run ubuntu bash -c 'echo $MYVAR'`?
1. Try again using the `--env MYVAR="hello world"` option to pass a variable to your container.

1. Create a simple file called myEnvFile that has one line: `ANOTHERVAR="hello mars"`
1. Use the `--env-file myEnvFile` option with `docker run ubuntu /bin/bash -c 'echo $ANOTHERVAR'`

```bash
export MY_HOST_VAR=dingo
docker run --env MY_HOST_VAR --env MY_CX_VAR=donkey ubuntu bash -c 'env | grep VAR'
```
